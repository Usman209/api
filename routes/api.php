<?php

use Illuminate\Http\Request;



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix'=>'products', 'middleware' => 'auth:api'],function(){

	Route::resource('/product','ProductController');
	Route::resource('/{product}/review','ReviewController');
});
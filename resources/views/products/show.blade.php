<h1>{{ $product->name }}</h1>

<ul>
	@foreach($product->reviews as $review)
		<li>{{ $review->customer }} - {{ $review->review }}</li>
	@endforeach
</ul>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	@guest
		<h1>You are not eligible to view</h1>
	@else
		@yield('content')
	@endguest
</body>
</html>